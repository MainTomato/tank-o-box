--by Alex Wordly
--http://pastebin.com/uVYpUtzF
--[[
    Reference:
function love.load()
    cam = require'amcam'()
    player = {
        x = 10,
        y = 20,
        vx = 0,
        vy = 0,
        angle = 0,
        img = love.graphics.newImage('player.png')
    }
    scale = 1
end
 
function love.update(dt)
    if love.keyboard.isDown('w') then player.vy = player.vy - 10*dt end
    if love.keyboard.isDown('a') then player.vx = player.vx - 10*dt end
    if love.keyboard.isDown('s') then player.vy = player.vy + 10*dt end
    if love.keyboard.isDown('d') then player.vx = player.vx + 10*dt end
    local oldx, oldy = player.x, player.y
    player.x, player.y = player.x + player.vx, player.y + player.vy
    player.angle = math.atan2(oldy - player.y, oldx - player.x)
    cam:setPosition(player.x, player.y)
    if love.keyboard.isDown('-') then scale = scale - 0.2 end
    if love.keyboard.isDown('=') then scale = scale + 0.2 end
    cam:setScale(scale)
end
 
function love.draw()
    cam:motor()
        love.graphics.rectangle('line', 0, 0, 800, 600)
        local width, height = player.img:getDimensions()
        love.graphics.draw(player.img, player.x, player.y, player.angle, _, _, width/2, height/2)
        local msx, msy = love.mouse.getPosition()
        local camx, camy = cam:toWorld(msx, msy)
        love.graphics.print('Mouse on screen: '..msx..':'..msy, camx, camy)
    cam:stop()
    love.graphics.print('This is demo')
end
]]

local camera = {}
local function isNum(v) return type(v) == 'number' end
local function err(v, ...) error(v:format(...), 3) end
local graphics = love.graphics or error'Graphics module required!'
setmetatable(camera,
    {__call =
        function(self)
            local o = {}
            o.x = 400
            o.y = 300
            o.gw, o.gh = graphics.getDimensions()
            o.scale = 1
            self.__index = self
            return setmetatable(o, self)
        end,
    }
)
 
function camera:moveTo(x, y)        -- move from current position to vector
    self.x = isNum(x) and self.x + x or err('Cam: arg#1 error: number expected, got '..type(x))
    self.y = isNum(y) and self.y + y or err('Cam: arg#2 error: number expected, got '..type(y))
end

function camera:setPosition(x, y)   --set absolute position
    self.x = isNum(x) and x or err('Cam: arg#1 error: number expected, got '..type(x))
    self.y = isNum(y) and y or err('Cam: arg#2 error: number expected, got '..type(y))
end
 

local function trim(x, min, max) return x < min and min or x > max and max or x end
  
function camera:getBounds()     -- get bounding box of camers
    local w, h = self.gw/(2*self.scale), self.gh/(2*self.scale)
    return self.x - w, self.y - h, self.x + w, self.y + h
end

function camera:getPosition()
    return self.x, self.y
end
 
function camera:toWorld(x, y)   -- translate screen position to camera
    local w, h = (x - self.gw/2) / self.scale, (y - self.gh/2) / self.scale
    return w + self.x, h + self.y
end
 
function camera:toCam(x, y)     -- translate camera position to world
	local x, y = x + self.x, y + self.y
	local w, h = graphics.getDimensions()
	return x - 0.5 * w / self.scale, y - 0.5 * h / self.scale
end
 
function camera:attach(t)
	if not t then self.attach = nil; return end
	if t.x and t.y then self.attach = t end
end
 
function camera:setScale(s)
    self.scale = isNum(s) and s or err('Cam: arg#1 error: number expected, got '..type(s))
end
 
function camera:getScale()
    return self.scale
end
 
function camera:motor()
    self.gw, self.gh = graphics.getDimensions()
		local w, h = graphics.getDimensions()
    graphics.push()
    graphics.scale(self.scale)
    graphics.translate(-self.x + (w/2)/self.scale, -self.y + (h/2)/self.scale)
end
 
function camera:stop()
    love.graphics.pop()
end

function camera:update()
	if self.attach then
		self:setPosition(self.attach.x, self.attach.y)
	end
end

return camera