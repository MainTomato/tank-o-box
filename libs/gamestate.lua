local help = [[ 
Reference manual:
 This lib provides state-automato into love2d, with autotracking 
 and autogenerating handlers, like love.update/love.mousemoved etc.
 Using:
 --initialisation state-system
 state = require 'states'
 
 --initialisation table for state #1
 local stateA = {}
 function stateA:load()
	self.x, self.y = 0.0
 end
 function stateA:update(dt)
	self.x, self.y = self.x + dt, self.y + dt^2
 end
 function stateA:draw()
	if love.keyboard.isDown('2') then state:set('B') end
	love.graphics.rectangle('line', self.x, self.y)
 end
 
 --initialisation table for state #2
 local stateB = {}
 function stateB:load()
	self.rect = {10, 20, 40, 30}
	self.bool = false
 end
 function stateB:keypressed(key, unicode, isrepeat) -- Any arguments that any version of LOVE send it
	if key == ' ' then self.bool = not self.bool end
	if key == '1' then state:set('A') end
 end
 function stateB:draw()
	if self.bool then
	 love.graphics.setColor(100, 200, 100)
	else
	 love.graphics.setColor(200, 100, 100)
	end
	love.graphics.rectangle('line', unpack(self.rect))
	
	-- attaching states:
	state:new('A', stateA)
	state:new('B', stateB)
	state:set('A')
 end
 
 
]]
local love = require'love'
if not love then error('This lib only for LOVE2D framework. download it here: https://love2d.org', 2) end
local callbacks = {'update', 'draw'}
--track love.handlers of current version of LOVE2d
for k, _ in pairs(love.handlers) do
	table.insert(callbacks, tostring(k))
end

local function isReserved(key)
	for i, v in ipairs(callbacks) do
		if key == v then return true end
	end
end

--Function returns new state like object
local function state(t, global)
	local s = t or {}
	s.State = global
	return s
end

--State manager
local states = {}
setmetatable(states,
	{__call =
		function(self)
			local o = {}
			o.state = {}                --state list
			o.current = state({}, self) --empty state
			o.currentname = 'nil'       --name for states:get()
			o.debug = true
			self.__index = self             --OOP
			return setmetatable(o, self)    --magic
		end
	}
)


function states:help()
	return help
end

--Self and additional love callback initialisation
--(load/update/draw/keypressed/keyreleased etc)
function states:init()
	for _, k in ipairs(callbacks) do
		--initialisation self methods like self:update(dt)
		self[k] = function(self, ...)
			if self.current[k] and type(self.current[k]) == 'function' then
				self.current[k](self.current, ...)
			end
		end
		--optional addition methods for love, if not defined
		if not love[k] then
			love[k] = function(...)
				self[k](self, ...)
			end
		end
	end
	return self
end

function states:new(n, t, ...)
	if type(n) ~= 'string' then error('Arg#1 State name expected, got '..type(n)) end
	if type(t) ~= 'table' then error('Arg#2 State table expected, got '..type(t)) end
	local st = state(t, self)
	self.state[n] = st
	if st.init and type(st.init) == 'function' then st:init(...) end
	return s
end

function states:set(name, ...)
	if not name or type(name) ~= 'string' or not self.state[name] then
		error('Name of defined state required, got: '..type(name)..':"'..tostring(name)..'"', 2)
	end
	if self.debug then love.window.setTitle(name) end
	if self.current.unload and type(self.current.unload) == 'function' then self.current:unload() end
	self.current = self.state[name]
	if self.current.load and type(self.current.load) == 'function' then self.current:load(...) end
	self.currentname = name
end

function states:get(name)
	if name and type(name) == 'string' and self.state[name] then return self.state[name] end
	return self.current, self.currentname
end

local s = states()
return s:init()