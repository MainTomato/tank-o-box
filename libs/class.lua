local function C(v, t) return type(v) == t and v or nil end -- checking shortcut
local function deepcopy(self)
	local mt = gmt(self)
	local copy = {}
	for k, v in pairs(self) do
		local type = type(v)
		if type == 'table' then
			copy[k] = cl.Copy(v)
		elseif type == 'cdata' then
			local c_clone = v.clone or v.copy
			if c_clone then 
				copy[k] = c_clone(v)
			end
		else
			copy[k] = v
		end
	end
	return mt and smt(copy, mt) or copy
end
 
local function Class(a, b)
  local smt, gmt = setmetatable, getmetatable
  local mt, cl = {}, {}
  mt.__index = C(a, 'table') or C(b, 'table')      -- parent class, if exists
  mt.__call = function(self, ...)
        local o = smt({}, cl)                      -- new obj
        if not cl.init then return o end           -- without init func - fine too
        return o, cl.init(o, ...)                  -- return obj and any returns in init func
    end
  cl.__name = C(a, 'string') or C(b, 'string') or 'class id:'..math.random(1e9-1)
  cl.__index = cl                              -- looping, but metamethod access
	cl.Class = cl
  cl.Super = mt.__index                        -- access to parent class. Super.Super.Super!
  cl.Type = function(self, obj) return C(obj, 'table') and gmt(obj) == gmt(self) or cl.__name end
  cl.IsObjectOf = function(self, class) return rawequal(class, gmt(self)) end
	cl.Copy = deepcopy
	
  return smt(cl, mt)
end

return Class
