local wall = require'libs/class'(require'data/entity', 'wall')
local vec = require'libs/ffivec'
wall.collision = {tag = {}, callback = {}}

wall:setCollisionTag('wall')

function wall:init(x, y, w, h, a)
	
	self.Super.init(self, 'rectangle', vec(x or 0, y or 0), vec(w or 60, h or 60), vec(1, 0):rotate(a))
end
--
function wall:remove()
	self:removeCollider()
end

local gr = love.graphics

function wall:draw()
	gr.push('all')
		love.graphics.setColor(0, 255, 255)
		self.shape:draw()
	gr.pop()
end

return wall