local game = {}


function game:init()
	self.world = require'data/world'
	self.player = self.world:newPlayer()

end

function game:update(dt)
	if not self.paused then
		self.world:update(dt)
	end
end
-- hmm
function game:draw()
	self.world:draw()
	if self.paused then
		love.graphics.print('Paused!')
	end
end

function game:keypressed(key)
	if key == 'escape' then self.paused = not self.paused end
end

function game:wheelmoved(x, y) 
	self.world:wheelmoved(x, y)
end

return game