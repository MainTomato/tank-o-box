local menu = {}

local function loop(n, min, max)
	n = n < min and max or n > max and min or n
	return n
end

function menu:init()
	--love.graphics.setDefaultFilter('nearest')
	self.colors = {
		selected = {255, 255, 255},
		disselected = {200, 200, 200}
	}
	
	self.list = {
		{title = 'Play!', func = function() self.State:set'game' end},
		{title = 'Exit',  func = love.event.quit}
	}
	
	self.current = 1
	
	self.font = love.graphics.newFont('data/visitor.ttf', 60)
	self.text = love.graphics.newText(self.font)
	
	love.keyboard.setKeyRepeat(1)
end

function menu:draw()
	local text = {}
	for i = 1, #self.list do
		text[#text + 1] = i == self.current and self.colors.selected or self.colors.disselected
		text[#text + 1] = self.list[i].title..'\n'
	end
	
	self.text:set(text)
	
	local w, h = love.graphics.getDimensions()
	local vx, vy = self.text:getDimensions()
	
	local t = math.sin(love.timer.getTime()/5)
	
	love.graphics.push()
		love.graphics.draw(self.text, 0, t * 30 + h/3, t/10, 1, 1, -vx/2, -vy/2)
	love.graphics.pop()
end

function menu:keypressed(key)
	if key == 'up'   then self.current = self.current + 1 end
	if key == 'down' then self.current = self.current - 1 end
	
	self.current = loop(self.current, 1, #self.list)
	
	if key == 'return' or key == 'spaced' then
		local f = self.list[self.current].func
		if type(f) == 'function' then f() end
	end
end

return menu