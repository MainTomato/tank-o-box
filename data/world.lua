local world = {}
local HC = require'libs/HC'
local tank = require'data/tank'
local wall = require'data/wall'
local entity = require'data/entity'
local vec = require'libs/ffivec'

function world:init()
	self.entity = {}
	self.players = {}
	self.collider = HC(100)
	self.cam = require'libs/hcam'()
	entity:setCollider(self.collider)
	
	tank:linkEnvironment(self.entity, self)
	for i = 1, 5 do
		self:spawn(tank(30 + i, 30, true))
	end
	
	--self:spawn(wall(30, 30, 10, 300, 1.2))
	
	return self
end

function world:newPlayer()
	local obj = self:spawn(tank(0, 0, false))
	self.players[#self.players + 1] = obj
	return obj
end

function world:spawn(obj)
	self.entity[#self.entity + 1] = obj
	return obj
end

function world:updatePlayers()
	local player = self.players[1]
	
	if love.keyboard.isDown('left',  'a') then player:rotate(-1)  end
	if love.keyboard.isDown('right', 'd') then player:rotate(1)   end
	if love.keyboard.isDown('up',    'w') then player:throttle(1) end
	if love.keyboard.isDown('down',  's') then player:brake(1)    end
	
	local vm = vec(self.cam:mousePosition())
	player:aim(vm.x, vm.y)
	
	self.cam:lookAt((player.pos + (vm - player.pos)/3):unpack())
end

function world:wheelmoved(x, y)
	local v = self.cam.scale + y/8
	v = v <= 0 and v + 1/8 or v > 2 and 2 or v
	self.cam:zoomTo(v)
end

function world.entitySorting(a, b)
	return a.pos.x < b.pos.x
end

function world:update(dt)
	table.sort(self.entity, self.entitySorting)
	for i, v in ipairs(self.entity) do v.index = i end
	for i, v in ipairs(self.entity) do
		if v.update then v:update(dt) end
	end
	self:updatePlayers()
end

function world:findNearest(obj, tag)
	local dist, nearest = math.huge
	local index = obj.index
	local list = self.entity
	local counter = 0
	for i = 1, math.max(index, #list - index) do
			counter = counter + 1
		local a = list[index - i]
		local b = list[index + i]
		if a then 
			if a.collision.tag[tag] then
				local d = obj.pos:dist2(a.pos)
				if dist > d then
					nearest = a; dist = d
				else
					break
				end
			end
		end
		if b then
			if b.collision.tag[tag] then
				local d = obj.pos:dist2(b.pos)
				if dist > d then
					nearest = b; dist = d
				else
					break
				end
			end
		end
	end
	return nearest
end

function world:draw()
	self.cam:attach()
	-- cutting drawing only for cam
	local x1, y1, x2, y2 = self.cam:getBoundingBox()
	local counter = 0
	for i, v in ipairs(self.entity) do
		if v.draw then
			local ms = math.max(v.size.x, v.size.y)
			if v.pos.x + ms > x1 and v.pos.x - ms < x2 and v.pos.y + ms > y1 and v.pos.y - ms < y2 then
				counter = counter + 1
				v:draw()
				love.graphics.print(i, v.pos:unpack())
			end
		end
	end
	love.graphics.rectangle('line', 0, 0, 800, 600)
	self.cam:detach()
	love.window.setTitle(counter..'::'..love.timer.getFPS())
end

return world:init()