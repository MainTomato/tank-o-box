local vec = require'libs/ffivec'

local tank = require'libs/class'(require'data/entity', 'tank')
tank:initClass()

tank:setCollisionTag('tank')
	
local coll = {}
function coll:tank(other, v)
	self.pos = self.pos + v/2
	-- local dot = self.angle:normalized():dot((other.pos - self.pos):normalized())
	-- if dot > 0 then self.spd = 0 end
end

function coll:wall(other, v)
	self.pos = self.pos + v
	local dot = self.angle:normalized():dot(v:normalized())
	self.spd = self.spd/2 * dot
end

tank:setCollisionCallback('tank', coll.tank)
tank:setCollisionCallback('wall', coll.wall)

function tank:init(x, y, isAi)
	self.Super.init(self, 'rectangle', vec(x or 0, y or 0), vec(60, 60), vec(1, 0))
	self.gun = vec(1, 0)
	self.spd = 0
	self.acc = 400
	self.hp = 10
	self.dt = 0
	self.dist = 0
	self.v_aim = vec(0, 0)
	self.ai = isAi
	self:setCollisionTag(isAi and 'enemy' or 'player')
end

function tank:remove()
	self:removeCollider()
end

function tank:useAI(dt)
	local player = self.world:findNearest(self, 'player')
	
	local a = (player.pos - self.pos):direction(self.angle)
	
	local dot = self.angle:perpendiculared(true):normalized()
								:dot((player.pos - self.pos):normalized())
	
	self:rotate(dot > 0 and 1 or -1)
	
	--print(('dot %0.3f'):format(dot))
	
	self:throttle(1 - math.abs(dot))
	
	self:aim(player.pos:unpack())
end

function tank:update(dt)
	self.dt = dt
	self.pos = self.pos + self.angle * self.spd * dt
	self.spd = self.spd - self.spd * 2 * dt
	self.dist = self.dist + self.spd * dt
	
	self.shape:setRotation(self.angle:angle())
	self.shape:moveTo(self.pos:unpack())
	self:resolveCollision()
	self:applyCollision()
	local dot = self.gun:perpendiculared(true):dot((self.v_aim - self.pos):normalized())
	if math.abs(dot) < 0.01 then
		self.gun:setAngle((self.v_aim - self.pos):angle())
	else
		self.gun:rotate(dot > 0 and 1 * dt or -1 * dt)
	end
	if self.ai == true then self:useAI(dt) end
end

function tank:aim(msx, msy)
	self.v_aim = vec(msx, msy)
end

function tank:rotate(angle)
	angle = type(angle) ~= 'number' and 1 or angle
	local v = self.angle:rotated(angle * self.dt)
	self.angle = v
	self.gun:rotate(angle * self.dt)
end

function tank:throttle(n)
	n = type(n) == 'number' and n or 1
	self.spd = self.spd + n * self.acc * self.dt
end

function tank:brake(n)
	self.spd = self.spd - n * self.acc / 2 * self.dt
end

local gr = love.graphics

function tank:draw()
	gr.points(self.pos.x, self.pos.y)
	gr.push('all')
		gr.translate(self.pos.x, self.pos.y)
		gr.rotate(self.angle:angle())
		local sx, sy = -self.size.x/2, -self.size.y/2
		local w, h = self.size.x, self.size.y
		
		gr.rectangle('line', sx, sy, w, h/4)				 -- left track
		gr.rectangle('line', sx , sy + w-h/4, w, h/4) -- right track
		
		gr.rectangle('fill', -sx, sy, 2, h) -- back
		
		gr.rotate(self.gun:angle() -self.angle:angle())
		gr.rectangle('line', sx/2, -sy/4, w, sy/2) -- gun
		
		
		gr.circle('line', 0, 0, sx/1.3)
	gr.pop()
	
	gr.push('all')
		love.graphics.setColor(255, 0, 255)
		love.graphics.circle('line', self.v_aim.x, self.v_aim.y, 5)
		self.shape:draw()
	gr.pop()
end

function tank:linkEnvironment(e, world)
	self.Class.environment = e
	self.Class.world = world
end

return tank