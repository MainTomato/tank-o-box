local entity = require'libs/class'(_, 'entity')
local vec = require'libs/ffivec'
entity.collider = {}


-- Entity initialisation, shape/pos/size/angle is vectors, it's important
function entity:init(shape, pos, size, angle)
	if not shape or not pos or not size or not angle then
		error('Entity cannot be created')
	end
	self.angle  = angle
	self.pos    = pos
	self.size   = size
	self.shape =  entity.collider[shape](entity.collider, pos:join(size))
	self.shape.object = self
	self.shape:setRotation(angle:angle())
	self.collision = {
		tag = setmetatable({}, {__index = self.Class.collision.tag}),
		callback = setmetatable({}, {__index = self.Class.collision.callback})
	}
end

-- Entity class collider initialisation, call after inheritanсe
function entity:initClass()
	self.collision = {
		callback = {},
		tag = {}
	}
end
-- 
-- If called by class - set global class tag, on object - set local object tag
function entity:setCollisionTag(tag, value)
	self.collision.tag[tag] = value or true
end

-- If called by class - set global class tag, on object - set local object tag
function entity:setCollisionCallback(tag, callback)
	self.collision.callback[tag] = callback
end

-- Must be called in botton of class-update function
function entity:resolveCollision()
	for shape, delta in pairs(self.collider:collisions(self.shape)) do
		local callbacks = {}
		
		-- global class callbacks
		for k, v in pairs(shape.object.Class.collision.tag) do
			callbacks[k] = v and self.collision.callback[k]
		end
		
		-- local object callbacks, overlap global
		for k, v in pairs(shape.object.collision.tag) do
			callbacks[k] = v and self.collision.callback[k]
		end
		
		for k, v in pairs(callbacks) do
			v(self, shape.object, vec(delta.x, delta.y))
		end
		
	end
end

-- Must be called in botton of class-update function
function entity:applyCollision()
	self.shape:moveTo(self.pos:unpack())
	self.shape:setRotation(self.angle:angle())
end

function entity:removeCollider(pos, angle)
	self.collider:remove(self.shape)
end

function entity:setCollider(hc)
	entity.collider = hc
end

function entity:setClassInit()
	self.collision = {
		tag      = {},
		callback = {},
	}
	
	entity.collider = hc
end

return entity